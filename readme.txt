Before installing this package you will need to install Apples Xcode command line tools and brew from https://brew.sh/ 
##########################################################################################
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)”
##########################################################################################
Copy and paste this command in terminal without the #


Next install the launcher build dependencies follow the guide from https://github.com/minecraft-linux/mcpelauncher-manifest/wiki/Compiling-from-sources

Example is running this command without the brackets [ brew install cmake qt libpng libzip libuv protobuf glfw ]

Then Proceed to run this Installer

To launch the launcher run [ mcpelauncher-ui-qt ] in terminal
